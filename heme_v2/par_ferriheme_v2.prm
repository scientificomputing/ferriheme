******************************************************
* Heme Solution Force Field (par_ferriheme_v2.prm)
*    for 5-coordinate Fe(III)PPIX 
* October 2014
* Citation: D. Kuter et al Inorg. Chem. 2014
* Comments and queries to Kevin J. Naidoo email:
*     kevin.naidoo(at)uct.ac.za
* Scientific Computing Reseach Unit (SCRU) at UCT
******************************************************
*
!References:
!A Oda, N Yamaotsu and S Hirono, J. Comput. Chem., 2004, 26(8), 818-826. 
!

ATOMS
!Hydrogens
MASS   166 HLW    1.00800 ! WATER and HYDRO LIGAND HYDROGEN (DKuter 18/04/12)
!Carbons
MASS   486 CPA   12.01100 ! heme alpha-C (top_all27_prot_na.rtf)
MASS   487 CPB   12.01100 ! heme beta-C (top_all27_prot_na.rtf)
MASS   488 CPM   12.01100 ! heme meso-C (top_all27_prot_na.rtf)
!Nitrogens
MASS   491 NPH   14.00700 ! heme pyrrole N (top_all27_prot_na.rtf)
!Oxygens
MASS   165 OLW   15.99940 ! WATER LIGAND OXYGEN (DKuter 17/05/12)
MASS   168 OLC   15.99940 ! HYDROXO LIGAND OXYGEN (DKuter 17/05/12)
MASS   169 OLH   15.99940 ! MUOXO LIGAND OXYGEN (DKuter 17/05/12)
!Iron
MASS   500 FE3   55.84700 ! Fe(III) ion for 5-coordinate heme (DKuter 18/04/12)

BONDS
!
CPB  CE1   450.000     1.3800 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPB  CPA   299.800     1.4432 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPB  CPB   340.700     1.3464 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPM  CPA   360.000     1.3716 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CT2  CPB   230.000     1.4900 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CT3  CPB   230.000     1.4900 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
HA   CPM   367.600     1.0900 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPA   377.200     1.3757 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  FE3   270.200     2.1800 ! Modified, originally taken from CHARMM22 FE-NPH (DKuter 10/04/12)
OLC  HLW   450.000     0.9572 ! TAKEN FROM OT HT (DKuter 17/05/12)
OLC  FE3   250.000     1.9300 ! Heme axial hydroxo ligand - modified, orginally taken from CHARMM22 FE-OM (DKuter 03/05/13)
OLH  FE3   230.000     1.9300 ! Heme axial mu-oxo ligand - modified, originally taken from CHARMM22 FE-OM (DKuter 03/05/13)
OLW  HLW   450.000     0.9572 ! TAKEN FROM OT HT (DKuter 17/05/12)
OLW  FE3   300.000     2.2500 ! Heme axial water ligand (DKuter 10/04/13) - modified, originally taken from FE-NPH 

ANGLES
!
HLW  OLW  HLW    45.000   114.0000 ! Heme axial water ligand (DKuter 17/05/12) - modified, orignially taken from TIP3P water
HA   CPM  CPA    12.700   117.4400 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
HA2  CT2  CPB    50.000   109.5000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
HA3  CT3  CPB    50.000   109.5000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
HE1  CE1  CPB    50.000   120.0000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPA  CPB  CE1    70.000   126.7400 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPA  CPM  CPA    94.200   125.1200 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPA  NPH  CPA   139.300   103.9000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPB  CE1  CE2    70.000   121.5000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPB  CPB  CE1    70.000   126.7500 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPB  CPB  CPA    30.800   106.5100 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CT2  CPB  CPB    65.000   126.7500 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CT2  CPB  CPA    65.000   126.7400 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CT2  CT2  CPB    70.000   113.0000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CT3  CPB  CPB    65.000   126.7500 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CT3  CPB  CPA    65.000   126.7400 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPM  CPA  CPB    61.600   124.0700 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPA  CPB   122.000   111.5400 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPA  CPM    88.000   124.3900 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  FE3  NPH    14.39     90.0000 ! Taken from NPH-FE-NPH DKuter 11/09/12
OLC  FE3  NPH   155.000   107.0000 ! Modified, TAKEN FROM OLH FE3 NPH (DKuter 03/08/12)
OLH  FE3  NPH   139.300   102.5000 ! Modified, orignially taken from S-FE-N, Oda et al (DKuter 03/05/13)
OLW  FE3  NPH   200.000   98.50000 ! Modified, originally taken from S-FE-N, Oda et al (DKuter 10/04/13)
FE3  NPH  CPA    96.15    128.0500 ! Taken from FE-N-C, Oda et al (DKuter 18/04/12)
FE3  OLC  HLW    33.000   128.0000 ! Modified, originally taken from FE-NR2-CPH1 (DKuter 10/04/10)
FE3  OLW  HLW    33.000   130.0000 ! Modified, originally taken from FE-NR2-CPH1 (DKuter 13/09/12)
FE3  OLH  FE3   120.000   180.0000 ! Modified, orignally taken from CHARMM22 OM-OM-FE (DKuter 03/05/12)

DIHEDRALS
!
HE2  CE2  CE1  CPB      5.2000  2   180.00 ! for vinyl, from butene, yin/adm jr., 12/95
HLW  OLC  FE3  NPH      0.0000  4     0.00 ! 
HLW  OLW  FE3  NPH      0.0000  2     0.00 ! 
X    CPA  CPB  X        0.0000  2     0.00 ! ALLOW HEM Heme (6-liganded): dummy for "auto dihe" (KK 05/13/91)
X    CPA  CPM  X        0.0000  2     0.00 ! ALLOW HEM Heme (6-liganded): dummy for "auto dihe" (KK 05/13/91)
X    CPB  CPB  X        0.0000  2     0.00 ! ALLOW HEM Heme (6-liganded): dummy for "auto dihe" (KK 05/13/91)
X    CPB  CT2  X        0.0000  6     0.00 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
X    CPB  CT3  X        0.0000  6     0.00 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
X    CE1  CPB  X        3.0000  2   180.00 !
X    NPH  CPA  X        0.0000  2     0.00 ! ALLOW HEM Heme (6-liganded): dummy for "auto dihe" (KK 05/13/91)
X    FE3  NPH  X        0.0000  2     0.00 ! ALLOW HEM Heme (6-liganded): for "ic para" only (KK 05/13/91)

IMPROPER
!
HA   CPA  CPA  CPM    29.4000         0      0.0000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPB  CPB  CPA  CE1    90.0000         0      0.0000 ! TAKEN FROM CPB-X-X-CE1 (DKuter 18/04/12)
CT2  X    X    CPB    90.0000         0      0.0000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CT3  X    X    CPB    90.0000         0      0.0000 ! ALLOW HEM Heme (6-liganded): substituents (KK 05/13/91)
CPB  CPA  NPH  CPA    20.8000         0      0.0000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPA  CPB  CPB    40.6000         0      0.0000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPA  CPM  CPA    18.3000         0      0.0000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPM  CPB  CPA    32.7000         0      0.0000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
NPH  CPA  CPA  FE3    18.3000         0      0.0000 ! Taken from NPH  CPA  CPM  CPA (DKuter 13/09/12)
OLW  FE3  HLW  HLW     0.0000         0      0.0000 ! 
OH1  X    X    CD    100.0000         0      0.0000 ! TAKEN FROM OB X X CD (DKuter 31/05/12)

NONBONDED nbxmod  5 atom cdiel shift vatom vdistance vswitch -
cutnb 14.0 ctofnb 12.0 ctonnb 10.0 eps 1.0 e14fac 1.0 wmin 1.5 
                !adm jr., 5/08/91, suggested cutoff scheme
!
!Carbons
CPA    0.000000  -0.090000     1.800000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPB    0.000000  -0.090000     1.800000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
CPM    0.000000  -0.090000     1.800000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
!Hydrogens
HLW    0.000000  -0.046000     0.224500 ! TAKEN FROM HT (DKuter 18/04/12)
!Nitrogens
NPH    0.000000  -0.200000     1.850000 ! ALLOW HEM Heme (6-liganded): porphyrin macrocycle (KK 05/13/91)
!Oxygens
OLC    0.000000  -0.152100     1.768200 ! TAKEN FROM OT (DKuter 17/05/12)
OLH    0.000000  -0.152100     1.768200 ! TAKEN FROM OT (DKuter 17/05/12)
OLW    0.000000  -0.152100     1.768200 ! TAKEN FROM OT (DKuter 17/05/12)
!Iron
FE3    0.000000  -0.020000     1.444300	! TAKEN FROM de HAtten et al for FE2+ (DKuter 17/05/12)
