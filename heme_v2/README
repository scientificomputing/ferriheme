######## CHARMM Ferriheme Force Field (v2) #############

##Purpose:
Parameter set for simulation of 5-coordinate iron(III)protoporphyrin IX species in aqueous solution

## Files:
ferriheme.inp
top_ferriheme_v2.rtf
par_ferriheme_v2.prm
cor_psf_files/hhem.cor
cor_psf_files/hhem.psf
cor_psf_files/mhem.cor
cor_psf_files/mhem.psf
cor_psf_files/whem.cor
cor_psf_files/whem.psf
cor_psf_files/whma.cor
cor_psf_files/whma.psf
cor_psf_files/whmd.cor
cor_psf_files/whmd.psf

## Reference:
D Kuter, V Streltzov, N Davydova, GA Venter, KJ Naidoo and TJ Egan, Inorg. Chem., 2014, 53, 10811-10824.
D Kuter, V Streltzov, N Davydova, GA Venter, KJ Naidoo and TJ Egan, J. Inorg. Biochem., 2015.

## Contact:
kevin.naidoo@uct.ac.za

## Changes to v2:
Ferriheme species are now entire residues (as opposed to having to be patched to HAEM in v1). As a result of this, relabeling of mu-oxo ferriheme (MHEM) was necessary. 
Atom numbering has been altered such that topology and parameter files can be used in conjunction with other CHARMM force fields.
Some modifications to parameters have been made:

  1. Aliphatic methylene and methyl hydrogens have been typed as HA2 and HA3 respectively in occordance with the CHARMM36 force field.
  2. FE3-OLH: 		250.0  1.93 --> 230.0  1.93 
  5. FE3-OLH-FE3:	200.0 180.0 --> 120.0 180.0 

## Implimentation
Ferriheme species are now built using in the standard CHARMM manner by generating the appropriate residue. Reading this force field in CHARMM must now be preceeded
by top_all36_prot.rtf and par_all36_prot.prm. See ferriheme.inp for examples on generating ferriheme species.
WHEM, WHMA, WHMD and HHEM still require patching to remove diagonal N-Fe-N bonds (FHEM), however, relabeling of mu-oxo ferriheme (MHEM) required creating a similar 
patch (FXHM) specifically for this species.  


